#ifndef XMODEM_H
#define XMODEM_H
#include <fstream>
#include "transport.h"
#include "buffer.h"

class Xmodem {
 public:
  enum TransferReturnCode{TRANSFER_SUCCESS = 0, TRANSFER_CANCELLED};
  Xmodem(Transport &t):transport_(t){}
  TransferReturnCode Send(const char* filename);
  TransferReturnCode Recv(const char* filename);
  
 private:
  enum BuildPacketReturns {PACKET_NOT_LAST = 0, LAST_PACKET};
  enum ParsePacketReturns {SUCCESS = 0, DUPLICATE, WRONG_CHECKSUM, WRONG_NUMBER, CANCELL, EOT_RECEIVED};
  enum ControlCharacterCode {ACK_RECEIVED = 0, NAK_RECEIVED, CAN_RECEIVED, DEFAULT};
  ParsePacketReturns ParsePacket();                            // parse packet procedure
  BuildPacketReturns BuildPacket();                            // build packet procedure
  ControlCharacterCode WaitControlCharacter();                 // wait control character procedure
  int pktsz_;                                                  // packet size
  char pktnum_;                                                // current packet number
  Transport& transport_;                                       // reference to the transport object
  Buffer buf_;                                                 // packet buffer
  std::fstream file_;                               
};
#endif
