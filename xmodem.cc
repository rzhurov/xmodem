#include <iostream>
#include <fstream>
#include <cstring>
#include <cassert>
#include "xmodem.h"

const char kSOH = 1;                        // start of header
const char kEOT = 4;                        // end of transmission
const char kACK = 6;                        // positive acknowledgment
const char kNAK = 21;                       // negative acknowledgment
const char kCAN = 24;                       // cancel process

namespace {
unsigned char CheckSum(char *s,int  l)           
{   
  unsigned char acc = 0;                           
  while (l--)                                  
    acc += *(unsigned char *) s++;           
  return(acc);                                    
} 
}

/* *****************************************
* 
* Send - send a file using XMODEM protocol
* 
*  returns:
*          0 - success
*          1 - cancelled
* 
* *****************************************/
Xmodem::TransferReturnCode Xmodem::Send(const char* filename)
{
  std::cout<<"Sender started"<<std::endl;
  file_.open(filename, std::fstream::binary|std::fstream::in);
  pktsz_ = 132;
  int state = 0;                                             
  buf_.StartOfHeader() = kSOH;
  pktnum_ = 1;  
  bool loop = true;
  bool end_reached = false;
  short error_count = 0;
  int ret = 0;
  unsigned int packets_sent = 0;
  while (loop)
  {
    switch (state)
    {
      case 0:                                                 // beginning of transmission              
        if (WaitControlCharacter() == NAK_RECEIVED)           // waiting NAK from receiver 
          state = 1;                                          // NAK received, going to build packet
        else
          state = 5;                                          // NAK not received, cancelling transmission
        continue;
      case 1:                                                 // building packet
        if (BuildPacket() == PACKET_NOT_LAST)
          state = 2;
        else {
          state = 2;
          end_reached = true;
        }
        continue;
      case 2:                                                 // sending the packet
        transport_.Send(&buf_.StartOfHeader(),pktsz_);
        state = 3;
        continue;
      case 3:                                                 // waiting ACK from receiver
        switch (WaitControlCharacter())
        {
          case ACK_RECEIVED:                                  //ACK received, going to send next packet, or end transmission
            std::cout<<"\rPackets sent: "<<++packets_sent;
            error_count = 0;
            state = end_reached ? 4 : 1;
            break;
          case NAK_RECEIVED:                                  //NAK received - resend packet
            std::cout<<std::endl<<"Retry "<<error_count<<": ";
            std::cout<<"NAK on sector";
            if(++error_count > 9){
              state = 5;
              break;
            }
            state = 2;                                        
            break;
          case CAN_RECEIVED:
            std::cout<<"CAN received";
            state = 5;
            break;
          case DEFAULT:
            assert(!DEFAULT);
        }
        continue;
      case 4:                                                 // end transmission
        transport_.Send(kEOT);                                // sending EOT
        if (WaitControlCharacter() == ACK_RECEIVED) {
          std::cout<<std::endl<<"Transfer complete"<<std::endl;     
          loop = 0;
        }
        else                                                
          state = 4;
        break;
      case 5:
        transport_.Send(kCAN);
        std::cout<<std::endl<<"Transfer incomplete"<<std::endl;
        loop = false;
        ret = 1;
        break;
    }   
  }
  file_.close();
  return ret ? TRANSFER_SUCCESS : TRANSFER_CANCELLED;
}
/* *****************************************
* 
* Recv - receive a file using XMODEM protocol
* 
*  returns:
*          0 - success
*          1 - cancelled
* 
* *****************************************/
Xmodem::TransferReturnCode Xmodem::Recv(const char* filename)
{
  std::cout<<"Receiver started"<<std::endl;
  file_.open(filename, std::fstream::binary|std::fstream::out|std::fstream::trunc);
  pktsz_ = 132;                               
  bool loop = true;
  int ret = 0;
  pktnum_ = 1;
  short error_count = 0;
  unsigned int packets_recvd = 0;
  transport_.Send(kNAK);
  while (loop)
  {
    switch(ParsePacket())
    {
      case SUCCESS:                                           
        file_.write(&buf_.Data(),128);
        std::cout<<"\rPackets received :"<<++packets_recvd;
        transport_.Send(kACK);
        break;
      case DUPLICATE:                                                       // duplicate packet received
        std::cout<<"Retry "<<error_count++<<": duplicate"<<std::endl;
        transport_.Send(kACK);
        break;
      case WRONG_CHECKSUM:                                                  // fail, retry
        std::cout<<"Retry "<<error_count++<<": wrong checksum"<<std::endl;
        transport_.Send(kNAK);
        break;
      case WRONG_NUMBER:                                                    // fail, retry
        std::cout<<"Retry "<<error_count++<<": wrong number"<<std::endl;
        transport_.Send(kNAK);
        break;
      case CANCELL:                                                         //CAN received
        std::cout<<"CAN received"<<std::endl;
        std::cout<<"Transfer incomplete"<<std::endl;
        //transport_.Send(kCAN);
        loop = 0;
        ret = 1;
        break;
      case EOT_RECEIVED:                                                    //end transmission
        std::cout<<std::endl<<"Transfer complete"<<std::endl;
        transport_.Send(kACK);
        loop = 0;
        ret = 0;
    }
  }
  file_.close();
  return ret ? TRANSFER_SUCCESS : TRANSFER_CANCELLED;
}
/* *****************************************
* 
*  ParsePacket - parse received packet
* 
*  returns:
*          0 - packet is right
*          1 - duplicate packet
*          2 - packet damaged
*          3 - CAN received or fatal transmission error
*          4 - EOT received
* 
* *****************************************/
Xmodem::ParsePacketReturns Xmodem::ParsePacket()
{
  transport_.Recv(&buf_.StartOfHeader(),1);                         // reading first byte from buffer
  switch(buf_.StartOfHeader())
  {
    case kCAN:
      return CANCELL;
    case kSOH:
      break;
    case kEOT:
      return EOT_RECEIVED;
  }
  transport_.Recv(&buf_.PacketNumber(),pktsz_-1);                  

  if (CheckSum(&buf_.Data(), 128) != (unsigned char) buf_.CheckSum())  {
    return WRONG_CHECKSUM;                                         // check sum is wrong. retry 
  }   
  if (buf_.PacketNumber() != ~buf_.PacketNumberComplement()) {     // is packet number consistent?   
    return WRONG_NUMBER;                                           // no, retry
  }
  if (buf_.PacketNumber() != pktnum_)                              // is packet number right?
  {                                                                // no, how much? 
    if (buf_.PacketNumber() == (pktnum_ - 1))                      // diff is one packet?
      return DUPLICATE;                                            // yes, skip current packet
     else   
      return CANCELL;                                              // no, abort transmission
  }    

  pktnum_++;
  return SUCCESS;
}

/* *****************************************
* 
*  WaitControlCharacter
* 
*  returns:
*          0 - ACK received
*          1 - NAK received
*          2 - CAN received
*          3 - default
* 
* *****************************************/
Xmodem::ControlCharacterCode Xmodem::WaitControlCharacter()
{
  char wc;
  transport_.Recv(&wc,1);
  switch(wc)
  {
    case kACK:
      return ACK_RECEIVED;
    case kNAK:
      return NAK_RECEIVED;
    case kCAN:
      return CAN_RECEIVED;
    default:
      return DEFAULT;
  }
}

/* *****************************************
* 
*  BuildPacket 
* 
*  returns:
*          0 - packet not last
*          1 - last packet
* 
* *****************************************/

Xmodem::BuildPacketReturns Xmodem::BuildPacket()
{
  buf_.PacketNumber() = pktnum_;                               
  buf_.PacketNumberComplement() = ~pktnum_++;                  
  memset(&buf_.Data(), 0, 128);                                
  file_.read(&buf_.Data(),128);
  buf_.CheckSum() = CheckSum(&buf_.Data(), 128);              
  if(file_.gcount()<128) {
    return LAST_PACKET;                                        //read less than 128 byte -> last packet
  }
  return PACKET_NOT_LAST;
}

