//
// base transport class
//
#ifndef TRANSPORT_H
#define TRANSPORT_H

class Transport
{
 public:
  virtual ~Transport(){};
  virtual void Send(const char* buf, int size) = 0;
  virtual void Send(const char c) = 0;
  virtual void Recv(char* buf, int size) = 0;
};
#endif
