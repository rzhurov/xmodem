//
// test transport through the file
//
// it`s recommended to use following command in terminal:
// socat PTY,link=filename1,raw,echo=0 PTY,link=filename2,raw,echo=0
//
// uses stdio, because there was a problem with fstream
//
#ifndef FILE_TRANSPORT_H
#define FILE_TRANSPORT_H
#include <cstdio>
#include "transport.h"

class FileTransport : public Transport {
 public:
  FileTransport(const char* f){
    fp=fopen(f, "a+");
  }
  virtual ~FileTransport(){
    fclose(fp);
  }
  virtual void Send(const char* buf, int size){
    fwrite(buf,sizeof(char),size,fp);
  }
  virtual void Send(const char c){
    fwrite(&c,sizeof(char),1,fp);
  }
  virtual void Recv(char* buf, int size){
    fread(buf,sizeof(char),size,fp);
  }
  
 private:
  FILE* fp;
};
#endif
