#ifndef BUFFER_H
#define BUFFER_H

class Buffer {
 public:
  char& StartOfHeader() {
    return buff_source_[0];
  }
  char& PacketNumber() {
    return buff_source_[1];
  }
  char& PacketNumberComplement() {
    return buff_source_[2];
  }
  char& Data() {
    return buff_source_[3];
  }
  char& CheckSum() {
    return buff_source_[131];
  }
  
private:
  char buff_source_[133];
};
#endif
